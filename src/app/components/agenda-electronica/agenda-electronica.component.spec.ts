import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaElectronicaComponent } from './agenda-electronica.component';

describe('AgendaElectronicaComponent', () => {
  let component: AgendaElectronicaComponent;
  let fixture: ComponentFixture<AgendaElectronicaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgendaElectronicaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaElectronicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
