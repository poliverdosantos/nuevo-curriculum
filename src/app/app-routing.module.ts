import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendaElectronicaComponent } from './components/agenda-electronica/agenda-electronica.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { FormularioContactoComponent } from './components/formulario-contacto/formulario-contacto.component';
import { NavbarComponent } from './components/navbar/navbar.component';

const routes: Routes = [
  { path: 'agenda', component: AgendaComponent },
  { path: 'agendaElectronica', component: AgendaElectronicaComponent },
  { path: 'formulario', component: FormularioContactoComponent },
  { path: 'nabvar', component: NavbarComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'nabvar' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
